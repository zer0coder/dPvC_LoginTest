package com.fgluver.logintest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class Login extends AppCompatActivity {

    static final Firebase fbase = new Firebase("https://shining-torch-1182.firebaseio.com/users/");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_login);
        Intent intent = getIntent();

        final String username = intent.getStringExtra(MainActivity.EXTRA_USERNAME);
        final String password = intent.getStringExtra(MainActivity.EXTRA_PASSWORD);

        //PROGRESS CHECKER
        System.out.println("Step 1: Initial Start");

        loginUser(username, password);
    }

    private void loginUser(final String username, final String password) {

        //INFORMATION RETRIEVING
        final TextView tv_user = (TextView)findViewById(R.id.text_username);
        final TextView tv_name = (TextView)findViewById(R.id.text_name);
        final TextView tv_email = (TextView)findViewById(R.id.text_email);
        final TextView tv_pass = (TextView)findViewById(R.id.text_password);

        //Firebase fbase = new Firebase("https://shining-torch-1182.firebaseio.com/users/");

        fbase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("There are: " + dataSnapshot.getChildrenCount() + " user(s)");
                for (DataSnapshot users : dataSnapshot.getChildren()) {
                    User uid = users.getValue(User.class);

                    if (users.getKey().equals(username)) {

                        //PROGRESS CHECKER
                        System.out.println("Step 2: Username found/accepted!");

                        if (uid.getPassword().equals(password)) {

                            //PROGRESS CHECKER
                            System.out.println("Step 3: Password for user accepted!");

                            System.out.println("Logged in!");
                            tv_user.setText(users.getKey());
                            tv_name.setText(uid.getName());
                            tv_email.setText(uid.getEmail());
                            tv_pass.setText(uid.getPassword());

                            //startMap();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });
    }

    protected void startMap() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
