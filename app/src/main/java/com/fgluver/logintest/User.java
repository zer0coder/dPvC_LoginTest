package com.fgluver.logintest;

/**
 * Created by Francisco on 04-09-2015.
 */
public class User {

    private String username;
    private String password;
    private String name;
    private String email;

    public User() {

    }
    public String getUsername() {
        return username;
    }
    public String getEmail() {
        return email;
    }
    public String getName() {
        return name;
    }
    public String getPassword() {
        return password;
    }
}
