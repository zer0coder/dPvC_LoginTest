package com.fgluver.logintest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.Objects;

public class SignUp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_sign_up);
    }

    public void registerUser(View view) {
        // New user information
        EditText username = (EditText) findViewById(R.id.edit_signup_username);
        EditText password = (EditText) findViewById(R.id.edit_signup_password);
        EditText firstname = (EditText) findViewById(R.id.edit_firstname);
        EditText lastname = (EditText) findViewById(R.id.edit_lastname);
        EditText email = (EditText) findViewById(R.id.edit_email);

        String newUser = username.getText().toString();
        String newUserPass = password.getText().toString();
        String newUserFirstname = firstname.getText().toString();
        String newUserLastname = lastname.getText().toString();
        String newUserEmail = email.getText().toString();

        registering(newUser, newUserPass, newUserFirstname, newUserLastname, newUserEmail);
    }

    public void registering(final String username, final String password, final String firstname, final String lastname, final String email) {
        final Firebase fbase = new Firebase("https://shining-torch-1182.firebaseio.com/users/");

        /**
         * Checking for existing users and their relative information (name, username and email).
         */

        // TODO: (Optional) Make a way to search and retrieve only username from the database.
        // TODO: (Optional) Improve checking of username method

        fbase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot users : dataSnapshot.getChildren()) {

                    User uid = users.getValue(User.class);
                    String keyname = users.getKey();
                    System.out.println(username + "  [COMPARE]  " + users.getKey());
                    System.out.println(email + "  [COMPARE]  " + uid.getEmail());

                    if(Objects.equals(username, keyname)) {
                        System.out.println("USER EXISTS! [COMPARE]");
                        setContentView(R.layout.activity_main);
                        finish();
                        return;
                    }
                    if(!Objects.equals(username, keyname)) {
                        Firebase reg = fbase.child(username);
                        reg.child("name").setValue(firstname + " " + lastname);
                        reg.child("email").setValue(email);
                        reg.child("password").setValue(password);
                        setContentView(R.layout.activity_main);
                        finish();
                        return;
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
