package com.fgluver.logintest;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.firebase.client.Firebase;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_USERNAME = "com.fgluver.logintest.USERNAME";
    public final static String EXTRA_PASSWORD = "com.fgluver.logintest.PASSWORD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);

        if (!isNetworkAvailable()) {
            System.exit(0);
        }

        // TODO: Improve UI for fun. Add in spinning circle for logging in.
        // TODO: Fix/Implement Login sequence for stay-logged-in and logout.

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void loginUser(View view) {
        Intent intent = new Intent(this, Login.class);
        EditText username = (EditText) findViewById(R.id.edit_username);
        EditText password = (EditText) findViewById(R.id.edit_password);
        intent.putExtra(EXTRA_USERNAME, username.getText().toString());
        intent.putExtra(EXTRA_PASSWORD, password.getText().toString());
        startActivity(intent);

        /** The following code checks if the user is tagged to be staying logged in until
        Intent intent = new Intent(this, Login.class);
        if(SaveSharedPreference.getUserName(MainActivity.this).length() == 0)
        {
            if(SaveSharedPreference.getPassWord(MainActivity.this).length() == 0) {
                EditText username = (EditText) findViewById(R.id.edit_username);
                EditText password = (EditText) findViewById(R.id.edit_password);
                intent.putExtra(EXTRA_USERNAME, username.getText().toString());
                intent.putExtra(EXTRA_PASSWORD, password.getText().toString());

                final CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_stayLogged);
                if (checkBox.isChecked()) {
                    SaveSharedPreference.setUserName(MainActivity.this, username.getText().toString());
                    SaveSharedPreference.setPassWord(MainActivity.this, password.getText().toString());
                }

                startActivity(intent);
            }
        } else {
            String user_logged = SaveSharedPreference.PREF_USER_NAME;
            String pass_logged = SaveSharedPreference.PREF_PASS;

            intent.putExtra(EXTRA_USERNAME, user_logged);
            intent.putExtra(EXTRA_PASSWORD, pass_logged);
            startActivity(intent);
        }
         **/

    }

    public void signUpUser(View view) {
        Intent intent = new Intent(this, SignUp.class);
        startActivity(intent);
    }

    // Map Testing Methods
    public void testMap(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
